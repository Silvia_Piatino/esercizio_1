#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

/// \brief ImportText import the text for encryption
/// \param inputFilePath: the input file path
/// \param text: the resulting text
/// \return the result of the operation, true is success, false is error
bool ImportText(const string& inputFilePath,
                string& text);

/// \brief Encrypt encrypt the text
/// \param text: the text to encrypt
/// \param password: the password for encryption
/// \param encryptedText: the resulting encrypted text
/// \return the result of the operation, true is success, false is error
bool Encrypt(const string& text,
             const string& password,
             string& encryptedText);

/// \brief Decrypt decrypt the text
/// \param text: the text to decrypt
/// \param password: the password for decryption
/// \param decryptedText: the resulting decrypted text
/// \return the result of the operation, true is success, false is error
bool Decrypt(const string& text,
             const string& password,
             string& decryptedText);

int main(int argc, char** argv) //argc è il numero di argomenti, compreso il nome; argv è array di stringhe e ogni stringa contiene gli argomenti. argv[0] contiene il nome del programma
{
  if (argc < 2) //il primo è il nome delleseguibile e il secondo è la password
  {
    cerr<< "Password shall passed to the program"<< endl;
    return -1;
  }
  string password = argv[1];
  //controllo se la password è tutta maiuscola
  for(int i=0; i<(int)password.length(); i++)
    if(!isupper(password[i]))
    {
        cerr<< "Password shall contain only uppercase letters"<< endl;
        return -1;
    }

  string inputFileName = "./text.txt", text; //sto dichiarando due variabili di tipo string

  if (!ImportText(inputFileName, text))
  {
    cerr<< "Something goes wrong with import"<< endl;
    return -1;
  }
  else
    cout<< "Import successful: result= "<< text<< endl;

  string encryptedText;
  if (!Encrypt(text, password, encryptedText)) //By reference
  {
    cerr<< "Something goes wrong with encryption"<< endl;
    return -1;
  }
  else
    cout<< "Encryption successful: result= "<< encryptedText<< endl;

  string decryptedText;
  if (!Decrypt(encryptedText, password, decryptedText) || text != decryptedText)
  {
    cerr<< "Something goes wrong with decryption"<< endl;
    return -1;
  }
  else
    cout<< "Decryption successful: result= "<< decryptedText<< endl;

  return 0;
}

bool ImportText(const string& inputFilePath,
                string& text)
{
   ifstream file;

   file.open(inputFilePath); // è un metodo di file. File è un oggetto, con metodi e attributi
   if(file.is_open()){
       getline(file, text);


       file.close();

       return true;
    }

   return false;
}

bool Encrypt(const string& text,
             const string& password,
             string& encryptedText)
{
    encryptedText = string(text.length(), ' '); //IMPORTANTE: per allocare memoria a una stringa, in modo dinamica, inizializzazione stringa
    for(int i=0; i<(int)text.length(); i++)
    {
        encryptedText[i]=(password[i%(int)password.length()]-64+text[i])%127;//tolgo 64 così A->1 B->2. %127 significa che se ottengo un numero maggiore di 127, gli sottraggo 95 per non avere i caratteri brutti
        if(encryptedText[i]<32)
            encryptedText[i]=encryptedText[i]+32; //aggiungo 32 così non ho i primi 32 brutti caratteri ascii
    }
  return true;
}

bool Decrypt(const string& text, //supposto che il carattere massimo è 126
             const string& password,
             string& decryptedText)
{
    decryptedText = string(text.length(), ' ');
    for(int i=0; i<(int)text.length(); i++)
    {
        decryptedText[i]=text[i]-password[i%(int)password.length()]+64;
        if(decryptedText[i]<32)
            decryptedText[i]+=95;
    }
  return true;
}
