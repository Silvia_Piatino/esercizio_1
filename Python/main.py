import sys # sys è una libreria per argv

# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text
def importText(inputFilePath):
    with open(inputFilePath, 'r') as file: # file è il puntatore al file che ho aperto
        return True, file.readline()[:-1] # IMPORTANTE: legge la riga scartando l'ultimo carattere, che è uno \n. Se esco vuol dire che c'è stato un errore

# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text
def encrypt(text, password): # sono entrambe stringhe. By reference
    encryptedText=[ord(i) for i in text] # lista di numeri. Comprehension. Ord trasforma carattere in numero. L'opposto è chr
    asciiPassword=[ord(i)-64 for i in password] # lista di numeri
    for j in range(len(text)): # se l'estremo inferiore è 0 si può omettere
        encryptedText[j]= (encryptedText[j]+asciiPassword[j%len(password)])%127 # IMPORTANTE in python esiste il += ma non il ++
        if encryptedText[j]<32:
            encryptedText[j]+=32
    # ora encryptedText è una lista di numeri. Voglio trasformarla in stringa
    encryptedText="".join([chr(i) for i in encryptedText])      # chr trasforma i numeri in caratteri. In python '' e "" sono intercambiabili. join è un metodo di una stringa (a sx) che unisce le stringhe di una lista (a dx). join è l'opposto di split

    return True, encryptedText

# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text
def decrypt(text, password): # stringhe
    decryptedText = [ord(i) for i in text]
    asciiPassword = [ord(i) - 64 for i in password]
    for j in range(len(text)):
        decryptedText[j] -= asciiPassword[j % len(password)]
        if decryptedText[j] < 32:
            decryptedText[j] += 95
    decryptedText = "".join([chr(i) for i in decryptedText])
    return True, decryptedText


if __name__ == '__main__':
    if len(sys.argv) < 2: # non c'è più argc. Gli sto chiedendo la lunghezza della prima dimensione, ovvero il numero di righe
        print("Password shall passed to the program")
        exit(-1)
    password = sys.argv[1]

    if not password.isupper():
        print("Password shall contain only uppercase letters")
        exit(-1)

    inputFileName = "text.txt"

    [resultImport, text] = importText(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text=", text)

    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result= ", encryptedText)

    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result= ", decryptedText)
