
# Password Encryption

Caesar encryption is easy to decipher because each letter of the text is shifted of the same quantity (`k`).
A safer method is to shift the text by a variable quantity dependent from an alphanumeric password

## Example 

Clear text: `ALICE I LOVE U`
Password: `GATTO`

Procedure:
```text
A	L	I	C	E		I		L	O	V	E		U
G	A	T	T	O	G	A	T	T	O	G	A	T	T
H	M	J	W	T	'	J	4	'	^	]	F	4	i
```

## Requirements

Write a program which reads a text from a file of name **text.txt** of the following format:

```text
This is a generic text to be encrypted
```
and a password written as an input argument (*command line arguments*) of the program.
The password shall contain only uppercase letters.

The program shall encrypt/decrypt the text and print both the results on screen.
